import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;



class PlayingCardTest {
    PlayingCard playingCard=new PlayingCard('H',10);
    @Test
    public void testGetAsString(){
        assertEquals(playingCard.getAsString(),"H10");
    }
    @Test
    public void testGetSuit(){
        assertEquals(playingCard.getSuit(),'H');
    }
    @Test
    public void testGetFace(){
        assertEquals(playingCard.getFace(),10);
    }
}


